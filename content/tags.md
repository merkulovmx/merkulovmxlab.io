---
title: "Tags"
date: 2020-07-18
draft: false
author: Maksim Merkulov
---

<!-- {{ range .Site.Taxonomies.tags.cmd }}
	// in here we can access the page’s information
	{{ .Title }}
{{ end }} -->

{{.Content}}
{{ range .Pages }}
	{{ .Title }}
{{ end }}