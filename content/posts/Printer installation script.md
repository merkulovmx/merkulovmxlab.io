---
title: "Printer installation script"
date: 2020-07-18
draft: false
author: Maksim Merkulov
tags: ["cmd","hardware"]
categories: ["scripting"]
moods: ["Happy", "Upbeat"]
---

How install all printers(drivers add, install, share) with script?
In Windows you can use scripts for printer installation.

1. Add driver
The name of printer driver you can get from a __driver.inf__ file

```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prndrvr.vbs" -a -m "Printer driver Universal V4 PCL" -i "\\Local_Network_folder\PRINTERS\Name_of_printer\Driver\x64\PCL6\KOBxxK__01.inf"
```

2. Add IP, Port
Port: 9100
IP address of the printer: 122.90.100.12
```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\Prnport.vbs" -a -r "Name_of_Driver_on_port" -h 122.90.100.12 -o raw -n 9100
```

3. Add installed printer in the printer list for IP and name

```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prnmngr.vbs" -a -p "New Printer in the Control Panel" -m "Printer driver Universal V4 PCL" -r "Name_of_Driver_on_port"
```

4. Shared its printer

```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prncnfg.vbs" -t -p "New Printer in the Control Panel" -h "New Printer Shared Name" +shared
```

## Some examples of a connection script

### Xerox 5225A
```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prndrvr.vbs" -a -m "Xerox WorkCentre 5225A" -i "\\Ntwrk\print_drivers\WC5225-5230_x64_ENG\x2GKOHL.inf"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\Prnport.vbs" -a -r "Xerox WorkCentre 5225A driver" -h 122.90.100.13 -o raw -n 9100

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prnmngr.vbs" -a -p "Xerox WorkCentre 5225A Name" -m "Xerox WorkCentre 5225A" -r "Xerox WorkCentre 5225A driver"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prncnfg.vbs" -t -p "Xerox WorkCentre 5225A Name" -h "Xerox WorkCentre 5225A shared" +shared
```

### Xerox VersaLink C7020
```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prndrvr.vbs" -a -m "Xerox VersaLink C7020 PCL6" -i "\\Ntwrk\print_drivers\VLC7000_5.617.7.0_PCL6_x64\VLC7000_5.617.7.0_PCL6_x64_Driver.inf\x3NMARX.inf"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\Prnport.vbs" -a -r "Xerox VersaLink C7020 driver" -h 122.90.100.14 -o raw -n 9100

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prnmngr.vbs" -a -p "Xerox VersaLink C7020 Name" -m "Xerox VersaLink C7020 PCL6" -r "Xerox VersaLink C7020 driver"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prncnfg.vbs" -t -p "Xerox VersaLink C7020 Name" -h "Xerox VersaLink C7020 shared" +shared
```

### Samsung
```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prndrvr.vbs" -a -m "Samsung CLX-3180 Series" -i "\\Ntwrk\print_drivers\Samsung CLX-3185\Samsung_CLX-3180_Series_SPD_V3.11.34.00.18\PRINTER\SPL_COLOR\WINXP_VISTA_64\sst2c.inf"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\Prnport.vbs" -a -r "Samsung CLX-3180 Series driver" -h 122.90.100.15 -o raw -n 9100

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prnmngr.vbs" -a -p "Samsung CLX-3180 Series Name" -m "Samsung CLX-3180 Series" -r "Samsung CLX-3180 Series driver"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prncnfg.vbs" -t -p "Samsung CLX-3180 Series Name" -h "Samsung CLX-3180 Series shared" +shared
```

### Kyocera FS-1135 MFP

```
cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prndrvr.vbs" -a -m "Kyocera FS-1135MFP KX" -i "\\Ntwrk\print_drivers\Kx630909_UPD_en\64bit\XP and newer\OEMSETUP.INF"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\Prnport.vbs" -a -r "Kyocera FS-1135MFP KX driver" -h 122.90.100.16 -o raw -n 9100

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prnmngr.vbs" -a -p "Kyocera FS-1135MFP Name" -m "Kyocera FS-1135MFP KX" -r "Kyocera FS-1135MFP KX driver"

cscript "C:\Windows\System32\Printing_Admin_Scripts\en-US\prncnfg.vbs" -t -p "Kyocera FS-1135MFP Name" -h "Kyocera FS-1135MFP shared" +shared
```