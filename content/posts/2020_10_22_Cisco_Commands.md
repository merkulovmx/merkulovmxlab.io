---
title: "Day-to-day Cisco commands"
date: 2020-10-22
draft: false
author: Maksim Merkulov
tags: ["cisco"]
categories: ["network"]
moods: ["Upbeat"]
---

### An list of commands 

 - See settings
```sh
#sh run int Fa0/24
Building configuration...

Current configuration : 213 bytes
!
interface FastEthernet0/324
description Bla_Bla port
switchport access vlan 1
switchport trunk native vlan 1
switchport trunk allowed vlan 1,2
switchport mode trunk
switchport nonegotiate
end
```
 - More settings from interface
```sh
#show int Fa0/24 switchport
```
 - All settings from interface
```sh
#show int Fa0/24
```

 - See all interfaces information
```sh
#sh int status
```

 - A standart list of vlans
```sh
#sh vl br
```
 - Show MAC address table
```sh
#show mac-address-table | include 0000.MAC0.0000
```

 - Show MAC address table on the port
```sh
#sh mac address-table interface Fa0/3
```

 - Look the connection devices
```sh
#sh cdp nei
```

 - How can search a forgotten device? (know MAC) OR How to find a device of a router from another router?
```sh
1. Main Cisco_0
#sh arp | i 33aa
Internet 100.100.2.20 0 0000.MAC0.33aa ARPA Vlan1
2. Detect the port
#sh mac address-table | i 33aa
VLAN1 0000.MAC0.33aa DYNAMIC Gi0/27 
Gi0/27 - it is an interface which are connecting with Cisco_1
3. Detected a name of Cisco_1
#show int Gi0/27
Description: Cisco_1
OR we can use other command
#show int Gi0/27 status
| Port | Name | Status | Vlan | Duplex | Speed |
| Gi0/27 | Cisco_1.Gi0/1 | connected | trunk | a-full | a-1000 |
4. Connect to Cisco_1
#sh mac address-table | i 33aa
VLAN1 0000.MAC0.33aa DYNAMIC Fa0/7 
```