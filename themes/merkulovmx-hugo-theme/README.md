# MerkulovMX blog - Hugo Theme

## Usage
Clone this repository into the themes/ folder. If you want you can rename it to something of your choosing, then modify the config.toml file with the following line:

theme = "merkulovmx-hugo-theme" 
