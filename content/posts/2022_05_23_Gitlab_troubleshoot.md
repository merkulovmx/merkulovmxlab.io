---
title: "Gitlab troubleshoot"
date: 2022-05-23
draft: false
author: Maksim Merkulov
tags: ["gitlab"]
categories: ["git"]
---

### 1. Issue disk is full
Error
```
“stderr: Failed to write to log, write /var/log/gitlab/gitlab-shell/gitlab-shell.log: no space left on device”
```

Check commands
```sh
df -h

lsof
docker info
docker ps -as
```

Solution

```sh
docker volume prune
docker system prune
```
