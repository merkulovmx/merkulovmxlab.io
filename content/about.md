---
title: "Who Am I"
date: 2020-09-02
draft: false
author: Maksim Merkulov
header: MR
---

 ![author](/static/images/author_circle.png)
__Information Technology Specialist (IT Specialist):__ Managing computer systems, networks to provide users good job conditions and developing scripts to automate routine processes.
I understood that all my life I like developing a code and more of my job achievements related to its. I am seeking employment with opportunities in Python programming. I want to focus my experience in real work tasks. I like to automate the job of the IT department and colleagues co-workers..

For instance, I was a participant who helped to integrate the 1C: ITIL  system for the Federal Taxes Systems IT department in 2015.  
I am currently serving in the field of IT in the automotive plant.  
I streamlined a maintain of the physical level of the network environment and improved it through the cameras and physical LAN routers installation in 2019.  
In addition, I developed a script on the Powershell for remote uninstall the old version of the special software and install it new for co-workers in 2020.  
Also, I made a Python script for data analysis that upload results to the SQL database in 2020.  

I am especially skilled in managing infrastructure and analyzing technical problems. My skills include computer programming and English (intermediate).

.....

Yours sincerely,  
Maksim Merkulov  
merkulovmx@gmail.com